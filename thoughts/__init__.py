from flask import Flask

from .config import Config

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_pagedown import PageDown
from flaskext.markdown import Markdown

app = Flask(__name__, static_folder="static")
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
pagedown = PageDown(app)
Markdown(app)

from .routes import *
from .models import *


@app.template_filter("username")
def user(obj):
    user = User.query.get(obj.user_id)
    return user.username


from datetime import datetime
from dateutil import parser


@app.template_filter("formatted_date")
def formatted_date(obj):
    date = obj.created_at
    return f"{date.day}/{date.month}/{date.year} at {date.hour}:{date.minute}:{date.second}"


@app.template_filter("len")
def count(obj):
    return obj.count()


@app.template_filter("short")
def thought(obj):
    t = Thought.query.get(obj.thought_id)
    return t.body[:100]
