from datetime import datetime

from .. import db


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    comment = db.Column(db.String(255))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    thought_id = db.Column(db.Integer, db.ForeignKey("thought.id"))
    created_at = db.Column(db.DateTime, default=datetime.now)
