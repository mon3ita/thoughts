from flask import render_template, redirect, url_for

from sqlalchemy import desc

from ..models import Comment, Thought, User
from .. import app, db

from ..forms import CommentForm

from . import login_required


@app.route(
    "/users/<int:user_id>/thoughts/<int:thought_id>/comment", methods=["GET", "POST"]
)
@login_required
def comment(user_id, thought_id):
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(
            comment=form.comment.data, user_id=user_id, thought_id=thought_id
        )
        db.session.add(comment)
        db.session.commit()
        return redirect(url_for("comments", thought_id=thought_id))
    return render_template("comment/create.html", form=form)


@app.route("/thoughts/<int:thought_id>/comments")
@login_required
def comments(thought_id):
    comments = Comment.query.filter_by(thought_id=thought_id).order_by(
        desc(Comment.created_at)
    )
    user_id = Thought.query.get(thought_id).user_id
    return render_template(
        "comment/index.html", comments=comments, user_id=user_id, thought_id=thought_id
    )


@app.route("/users/<int:user_id>/comments")
@login_required
def user_comments(user_id):
    user = User.query.get(user_id)
    comments = Comment.query.filter_by(user_id=user_id).order_by(
        desc(Comment.created_at)
    )
    return render_template("comment/user.html", comments=comments, user=user)
