from flask import render_template, redirect, url_for
from flask_login import current_user, login_user, logout_user

from .. import app, login, db
from ..forms.user import LoginForm, RegisterForm, UserForm
from ..models import User, Thought

from . import login_required


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for("index"))
    return render_template("user/login.html", form=form)


@app.route("/register", methods=["GET", "POST"])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for("index"))
    return render_template("user/signup.html", form=form)


@app.route("/users/edit", methods=["GET", "POST"])
@login_required
def update():
    user = User.query.get(current_user.id)
    form = UserForm(username=user.username, email=user.email)
    if form.validate_on_submit():
        user.username = form.username.data
        user.email = form.email.data
        user.set_password(form.password.data)
        db.session.commit()
        return redirect(url_for("index"))
    return render_template("user/edit.html", form=form)


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))


@app.route("/users", methods=["GET"])
@login_required
def all():
    users = User.query.all()
    return render_template("user/index.html", users=users)
