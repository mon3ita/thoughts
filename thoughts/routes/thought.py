from flask import render_template, redirect, url_for, request

from sqlalchemy import desc

from .. import app, db

from ..models import Thought, User
from ..forms import ThoughtForm

from . import login_required


@app.route("/thoughts/<int:user_id>/create", methods=["GET", "POST"])
@login_required
def create(user_id):
    form = ThoughtForm()
    if form.validate_on_submit():
        thought = Thought(body=form.body.data, user_id=user_id)
        db.session.add(thought)
        db.session.commit()
        return redirect(url_for("index"))
    return render_template("thoughts/create.html", form=form)


@app.route("/thoughts/<int:user_id>/edit/<int:thought_id>", methods=["GET", "POST"])
@login_required
def edit(user_id, thought_id):
    thought = Thought.query.get(thought_id)
    form = ThoughtForm(id=thought, body=thought.body)
    if form.validate_on_submit():
        thought.body = form.body.data
        db.session.commit()
        return redirect(url_for("index"))
    return render_template("thoughts/edit.html", form=form)


@app.route("/thoughts/<int:user_id>/delete/<int:thought_id>")
@login_required
def delete(user_id, thought_id):
    thought = Thought.query.get(thought_id)
    db.session.delete(thought)
    db.session.commit()
    return redirect(url_for("index"))


@app.route("/users/<int:user_id>", methods=["GET"])
@login_required
def user_thoughts(user_id):
    user = User.query.get(user_id)
    thoughts = Thought.query.filter_by(user_id=user_id).order_by(
        desc(Thought.created_at)
    )
    return render_template("thoughts/index.html", user=user, thoughts=thoughts)
