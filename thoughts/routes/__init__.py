from functools import wraps

from flask import redirect, url_for
from flask_login import current_user


def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for("login"))
        return func(*args, **kwargs)

    return wrapper


from .thought import *
from .user import *
from .home import *
from .comment import *
