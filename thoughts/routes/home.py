from flask import render_template
from flask_login import current_user
from sqlalchemy import desc

from ..models import Thought
from .. import app

from . import login_required


@app.route("/index")
@app.route("/")
@login_required
def index():
    thoughts = []
    if current_user.is_authenticated:
        thoughts = Thought.query.filter_by(user_id=current_user.id).order_by(
            desc(Thought.created_at)
        )
    return render_template("home/index.html", thoughts=thoughts)
