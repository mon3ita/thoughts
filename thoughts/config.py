from nonsensepy import NonsensePyGen as ns
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = ns.strrandom(min=50, max=100)
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "app.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
