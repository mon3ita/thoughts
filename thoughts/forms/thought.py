from flask_wtf import FlaskForm
from flask_pagedown.fields import PageDownField
from wtforms import SubmitField
from wtforms.validators import DataRequired


class ThoughtForm(FlaskForm):
    body = PageDownField("Description", [DataRequired()])
    submit = SubmitField("Submit")
